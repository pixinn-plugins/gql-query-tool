"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Transaction = void 0;
const graphql_tag_1 = __importDefault(require("graphql-tag"));
const utils_1 = require("../utils");
class Transaction {
    /**
     * Creates transaction container.
     * @param name transaction name. Valid regex format (^[a-zA-Z_][a-zA-Z0-9_]*$).
     * @param type transaction type.
     */
    constructor(name, type) {
        this.name = name;
        this.type = type;
        this.queries = [];
    }
    /**
     * Adds queries to tranasaction.
     * @param queries queries to add to transaction.
     */
    add(...queries) {
        this.queries.push(...queries);
        return this;
    }
    /**
     * Parses transaction to GraphQL DocumentNode format.
     */
    _parse() {
        if (!this.queries.length) {
            throw new Error('Transaction must have at least one query or mutation');
        }
        if (!utils_1.aliasValidityRegex.test(this.name)) {
            throw new Error('Transaction name format is invalid.');
        }
        return graphql_tag_1.default `${this.type} ${this.name}{${this.queries.map(query => query._parse())}}`;
    }
    /**
     * Return string representation of parsed Queries.
     */
    toString() {
        return `${this.type} ${this.name}{${this.queries.map(query => query._parse())}}`;
    }
}
exports.Transaction = Transaction;
