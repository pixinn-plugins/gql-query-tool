import { Transaction } from './transaction';
import { GQL_Base } from '../gql-base-typings';
export interface Mutation {
    add(...params: Parameters<Transaction['add']>): Mutation;
}
export declare class Mutation extends Transaction {
    constructor(name: string, ...queries: GQL_Base.Parseable[]);
}
