import { DocumentNode } from 'graphql';
import { GQL_Base } from '../gql-base-typings';
export declare abstract class Transaction implements GQL_Base.Parseable<DocumentNode> {
    private type;
    private name;
    private queries;
    /**
     * Creates transaction container.
     * @param name transaction name. Valid regex format (^[a-zA-Z_][a-zA-Z0-9_]*$).
     * @param type transaction type.
     */
    constructor(name: string, type: 'mutation' | 'query');
    /**
     * Adds queries to tranasaction.
     * @param queries queries to add to transaction.
     */
    add(...queries: GQL_Base.Parseable[]): Transaction;
    /**
     * Parses transaction to GraphQL DocumentNode format.
     */
    _parse(): DocumentNode;
    /**
     * Return string representation of parsed Queries.
     */
    toString(): string;
}
