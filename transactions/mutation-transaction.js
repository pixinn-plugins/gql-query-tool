"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mutation = void 0;
const transaction_1 = require("./transaction");
class Mutation extends transaction_1.Transaction {
    constructor(name, ...queries) {
        super(name, 'mutation');
        this.add(...queries);
    }
}
exports.Mutation = Mutation;
