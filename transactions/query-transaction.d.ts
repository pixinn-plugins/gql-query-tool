import { Transaction } from './transaction';
import { GQL_Base } from '../gql-base-typings';
export interface Query {
    add(...params: Parameters<Transaction['add']>): Query;
}
export declare class Query extends Transaction {
    constructor(name: string, ...queries: GQL_Base.Parseable[]);
}
