"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Query = void 0;
const transaction_1 = require("./transaction");
class Query extends transaction_1.Transaction {
    constructor(name, ...queries) {
        super(name, 'query');
        this.add(...queries);
    }
}
exports.Query = Query;
