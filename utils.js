"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.aliasValidityRegex = exports.applyMixins = exports.escape = void 0;
function escape(value) {
    if (value === undefined || value === null) {
        return null;
    }
    if (value instanceof Date) {
        return `"${value.toISOString()}"`;
    }
    if (typeof value === 'string') {
        return `"${value
            .split(/\\"|"/)
            .join('\\"')
            .split(/\\'|'/)
            .join('\\\'')}"`;
    }
    if (['number', 'boolean'].includes(typeof value)) {
        return value;
    }
    return value;
}
exports.escape = escape;
function applyMixins(derivedCtor, ...constructors) {
    constructors.forEach((baseCtor) => {
        if (baseCtor) {
            Object.getOwnPropertyNames(baseCtor.prototype).forEach((name) => {
                Object.defineProperty(derivedCtor.prototype, name, Object.getOwnPropertyDescriptor(baseCtor.prototype, name));
            });
        }
    });
}
exports.applyMixins = applyMixins;
exports.aliasValidityRegex = /^[a-zA-Z_][a-zA-Z0-9_]*$/;
