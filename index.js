"use strict";
// tslint:disable-next-line:no-reference
/// <reference path="../typings/gql-declarations.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var query_1 = require("./queries/query");
Object.defineProperty(exports, "Query", { enumerable: true, get: function () { return query_1.Query; } });
Object.defineProperty(exports, "TypedQuery", { enumerable: true, get: function () { return query_1.TypedQuery; } });
var aggregate_1 = require("./queries/aggregate");
Object.defineProperty(exports, "Aggregate", { enumerable: true, get: function () { return aggregate_1.Aggregate; } });
Object.defineProperty(exports, "TypedAggregate", { enumerable: true, get: function () { return aggregate_1.TypedAggregate; } });
var delete_1 = require("./queries/delete");
Object.defineProperty(exports, "Delete", { enumerable: true, get: function () { return delete_1.Delete; } });
Object.defineProperty(exports, "TypedDelete", { enumerable: true, get: function () { return delete_1.TypedDelete; } });
var update_1 = require("./queries/update");
Object.defineProperty(exports, "Update", { enumerable: true, get: function () { return update_1.Update; } });
Object.defineProperty(exports, "TypedUpdate", { enumerable: true, get: function () { return update_1.TypedUpdate; } });
var insert_1 = require("./queries/insert");
Object.defineProperty(exports, "Insert", { enumerable: true, get: function () { return insert_1.Insert; } });
Object.defineProperty(exports, "TypedInsert", { enumerable: true, get: function () { return insert_1.TypedInsert; } });
var query_transaction_1 = require("./transactions/query-transaction");
Object.defineProperty(exports, "QueryContainer", { enumerable: true, get: function () { return query_transaction_1.Query; } });
var mutation_transaction_1 = require("./transactions/mutation-transaction");
Object.defineProperty(exports, "MutationContainer", { enumerable: true, get: function () { return mutation_transaction_1.Mutation; } });
