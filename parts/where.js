"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Where = void 0;
const utils_1 = require("../utils");
class Where {
    constructor() {
        this.where = this.where.bind(this);
    }
    get getWhere() {
        return this._where;
    }
    get getParsedWhere() {
        const where = this.getWhere;
        if (!where) {
            return undefined;
        }
        const handler = Where.recursionWhere;
        return handler('where', where);
    }
    where(condition) {
        this._where = condition;
        return this;
    }
    static recursionWhere(key, value) {
        /** _not, _eq, _neq, _is_null, _gt, _gte, _lt, _lte, _like, _ilike, _nlike, _nilike */
        if (value instanceof Date || typeof value !== 'object') {
            const val = utils_1.escape(value);
            const operation = Ops.includes(key);
            return `${key}:${operation ? val : `{_eq:${val}}`}`;
        }
        /** _in, _nin, _or, _and */
        if (Array.isArray(value)) {
            return `${key}:[${value
                .map(val => {
                /** _in, _nin */
                if (val instanceof Date || typeof val !== 'object') {
                    return utils_1.escape(val);
                }
                /** _or, _and */
                return Object.keys(val)
                    .map(k => `{${Where.recursionWhere(k, val[k])}}`)
                    .join(',');
            })
                .join(',')}]`;
        }
        /** Nesting */
        return `${key.toString()}:{${Object.keys(value || {})
            .map((k) => Where.recursionWhere(k, value[k]))
            .join(',')}}`;
    }
}
exports.Where = Where;
const Ops = ['_or', '_and', '_not', '_eq', '_neq', '_in', '_nin', '_is_null', '_gt', '_gte', '_lt', '_lte', '_like', '_ilike', '_nlike', '_nilike'];
