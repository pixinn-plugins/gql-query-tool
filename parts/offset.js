"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Offset = void 0;
class Offset {
    constructor() {
        this.offset = this.offset.bind(this);
    }
    get getOffset() {
        return this._offset;
    }
    get getParsedOffset() {
        const offset = this.getOffset;
        if (!offset) {
            return undefined;
        }
        if (offset < 0) {
            throw new Error(`Query offset cannot be negative.`);
        }
        return `offset:${offset}`;
    }
    offset(amount) {
        this._offset = amount;
        return this;
    }
}
exports.Offset = Offset;
