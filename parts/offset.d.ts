export declare class Offset {
    constructor();
    private _offset?;
    get getOffset(): number | undefined;
    get getParsedOffset(): string | undefined;
    offset(amount: number): any;
}
