export declare class Limit {
    constructor();
    private _limit?;
    get getLimit(): number | undefined;
    get getParsedLimit(): string | undefined;
    limit(amount: number): any;
}
