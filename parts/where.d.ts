import { GQL_Base, GQL_Types, GQL_Model } from '../gql-base-typings';
export declare class Where<T extends GQL_Base.ViewOrModel> {
    constructor();
    private _where?;
    get getWhere(): ConditionSymbolics<GQL_Types.RemoveTypename<T>> | WhereClause<GQL_Types.RemoveTypename<Pick<{ [K in keyof T]: T[K] extends GQL_Model.Aggregate<any> ? never : T[K]; }, { [K_1 in keyof { [K in keyof T]: T[K] extends GQL_Model.Aggregate<any> ? never : T[K]; }]: { [K in keyof T]: T[K] extends GQL_Model.Aggregate<any> ? never : T[K]; }[K_1] extends never ? never : K_1; }[keyof T]>>> | undefined;
    get getParsedWhere(): string | undefined;
    where(condition: any): any;
    static recursionWhere(key: string, value: IWhere<any>): string;
}
declare type OmitAggregate<T> = GQL_Types.PickValues<{
    [K in keyof T]: T[K] extends GQL_Model.Aggregate<any> ? never : T[K];
}>;
export declare type IWhere<T> = ConditionSymbolics<GQL_Types.RemoveTypename<T>> | WhereClause<GQL_Types.RemoveTypename<OmitAggregate<T>>>;
declare type WhereClause<T> = {
    [K in keyof T]?: T[K] extends GQL_Types.SimpleTypes ? ConditionSymbolics<T[K]> | Val<T[K]> | null : T[K] extends Array<infer R> ? IWhere<R> : IWhere<T[K]>;
};
declare type Val<T> = T | (T extends number | Date ? NumericSymbolics<T> : T extends string ? StringSymbolics<T> : GenericSymbolics<T>);
declare type ConditionSymbolics<Type> = Type extends object ? {
    /** Or condition join */
    _or?: Type extends GQL_Types.SimpleTypes ? Type[] : Array<IWhere<Type>>;
    /** And condition join (default) */
    _and?: Type extends GQL_Types.SimpleTypes ? Type[] : Array<IWhere<Type>>;
    /** Condition invertion. (Ex. Get all users where user is NOT female) */
    _not?: Type extends GQL_Types.SimpleTypes ? Type : IWhere<Type>;
} : {};
interface GenericSymbolics<Type> {
    /** Field is equal to value */
    _eq?: Type;
    /** Field is not equal to value */
    _neq?: Type;
    /** Field value is in given array of values */
    _in?: Type[];
    /** Field value is not in given array of values */
    _nin?: Type[];
    /** Is field is null */
    _is_null?: boolean;
}
interface NumericSymbolics<Type> extends GenericSymbolics<Type> {
    /** Field value is greater than given value */
    _gt?: Type;
    /** Field value is greater than or equal to given value */
    _gte?: Type;
    /** Field value is less than given value */
    _lt?: Type;
    /** Field value is less than or equal to given value */
    _lte?: Type;
}
interface StringSymbolics<Type> extends GenericSymbolics<Type> {
    /** Field have given substring */
    _like?: Type;
    /** Field have given substring. Case insensitive */
    _ilike?: Type;
    /** Field don't have given substring */
    _nlike?: Type;
    /** Field don't have given substring. Case insensitive */
    _nilike?: Type;
}
export {};
