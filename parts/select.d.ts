import { QueryBase } from '../queries/base';
import { GQL_Base, GQL_Types } from '../gql-base-typings';
export declare class Select<T extends GQL_Base.ViewOrModel> {
    constructor();
    private _select?;
    get getSelect(): SelectType<T, any>[] | undefined;
    get getParsedSelect(): string;
    select(...fields: Array<GQL_Types.Keys<T> | Alias<T>>): any;
    _join(field: any, Q: new (table: string, alias?: string) => QueryBase, cb: (model: any) => void): any;
}
declare type Join<T extends object, Q> = [GQL_Types.Objects<T>, string, Q];
declare type Alias<T extends object> = [GQL_Types.Keys<T>, string];
declare type SelectType<T extends object, Q> = GQL_Types.Keys<T> | Alias<T> | Join<T, Q>;
export {};
