import { GQL_Base, GQL_Model, GQL_Types } from '../gql-base-typings';
export declare class Order<T extends GQL_Base.ViewOrModelOrAggregate> {
    constructor();
    private _order?;
    get getOrder(): Array<IOrder<T>> | undefined;
    get getParsedOrder(): string | undefined;
    order(...order: Array<IOrder<T>>): any;
    private parseSingleOrder;
}
declare type PickSimpleTypes<T extends object> = Pick<T, GQL_Types.Keys<T>>;
declare type NumericFields<T extends object> = {
    [K in keyof T]: T[K] extends number ? K : never;
}[keyof T];
declare type NumericOrder<T extends GQL_Model.Model> = Pick<IOrder<T>, Exclude<keyof IOrder<T>, Exclude<keyof IOrder<T>, NumericFields<T>>>>;
interface AggregationOrder<T extends GQL_Model.Model> {
    count?: OrderTypes;
    min?: PickSimpleTypes<IOrder<T>>;
    max?: PickSimpleTypes<IOrder<T>>;
    avg?: NumericOrder<T>;
    sum?: NumericOrder<T>;
}
export declare type IOrder<T extends GQL_Base.ViewOrModelOrAggregate> = Partial<GQL_Types.PickValues<{
    [K in Exclude<keyof T, keyof GQL_Base.ViewOrModelOrAggregate>]: T[K] extends GQL_Types.SimpleTypes ? OrderTypes : T[K] extends any[] ? never : T[K] extends GQL_Base.ViewOrModel ? IOrder<T[K]> : T[K] extends GQL_Model.Aggregate<infer R> ? AggregationOrder<R> : never;
}>>;
export declare type OrderTypes = 'asc' | 'asc_nulls_first' | 'asc_nulls_last' | 'desc' | 'desc_nulls_first' | 'desc_nulls_last';
export {};
