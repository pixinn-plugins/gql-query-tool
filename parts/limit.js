"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Limit = void 0;
class Limit {
    constructor() {
        this.limit = this.limit.bind(this);
    }
    get getLimit() {
        return this._limit;
    }
    get getParsedLimit() {
        const limit = this.getLimit;
        if (!limit) {
            return undefined;
        }
        if (limit < 0) {
            throw new Error(`Query limit cannot be negative.`);
        }
        return `limit:${limit}`;
    }
    limit(amount) {
        this._limit = amount;
        return this;
    }
}
exports.Limit = Limit;
