"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Select = void 0;
class Select {
    constructor() {
        this.select = this.select.bind(this);
        this._join = this._join.bind(this);
    }
    get getSelect() {
        return this._select;
    }
    get getParsedSelect() {
        var _a;
        const select = (_a = this.getSelect) === null || _a === void 0 ? void 0 : _a.filter(f => Array.isArray(f) || typeof f === 'string');
        if (!select || select.length === 0) {
            throw new Error('Query must select parameters');
        }
        return select
            .map(f => Array.isArray(f)
            ? f.length === 2
                ? `${f[1]}:${f[0]}`
                : f[2]._parse()
            : f)
            .join(',');
    }
    select(...fields) {
        if (this._select) {
            this._select.push(...fields);
        }
        else {
            this._select = fields;
        }
        return this;
    }
    _join(field, Q, cb) {
        var _a;
        const table = Array.isArray(field) ? field[0] : field;
        const alias = Array.isArray(field) ? field[1] : field;
        const SubQuery = new Q(table, alias);
        cb(SubQuery);
        const item = [field, alias, SubQuery];
        if (this._select) {
            (_a = this._select) === null || _a === void 0 ? void 0 : _a.push(item);
        }
        else {
            this._select = [item];
        }
        return this;
    }
}
exports.Select = Select;
