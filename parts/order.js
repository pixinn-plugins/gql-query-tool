"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Order = void 0;
class Order {
    constructor() {
        this.order = this.order.bind(this);
        this.parseSingleOrder = this.parseSingleOrder.bind(this);
    }
    get getOrder() {
        return this._order;
    }
    get getParsedOrder() {
        const order = this.getOrder;
        if (!order) {
            return undefined;
        }
        if (order.length === 0) {
            throw new Error('Order property cannot be empty');
        }
        return `order_by:[${order
            .map((item) => this.parseSingleOrder(item))
            .join(',')}]`;
    }
    order(...order) {
        this._order = order;
        return this;
    }
    parseSingleOrder(item) {
        const keys = Object.keys(item);
        if (keys.length > 1) {
            throw new Error('Order object accepts only one order field. If you want to add several fields to order, pass another order object to order function');
        }
        const key = keys[0];
        if (!key) {
            throw new Error('Order must have field key by which it will order');
        }
        const value = item[key];
        if (!value) {
            throw new Error('Must have order direction');
        }
        if (typeof value === 'string') {
            return `{${key}:${value}}`;
        }
        return `{${key}:${this.parseSingleOrder(value)}}`;
    }
}
exports.Order = Order;
