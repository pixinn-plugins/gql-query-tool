"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypedInsert = exports.Insert = void 0;
const mutation_base_1 = require("./mutation-base");
const query_1 = require("./query");
const aggregate_1 = require("./aggregate");
const select_1 = require("../parts/select");
const utils_1 = require("../utils");
class Insert extends mutation_base_1.MutationBase {
    constructor(table, alias) {
        super(table, alias);
        this.join = this.join.bind(this);
        this.aggregate = this.aggregate.bind(this);
    }
    _parse() {
        const select = this.getParsedSelect;
        const recusiveParse = (prefix, ...queries) => {
            const isArray = queries.length > 1;
            let innards = queries.map(query => {
                const fields = Object.keys(query);
                return fields.map(field => {
                    const value = query[field];
                    if (value instanceof Date || (typeof value !== 'object' && typeof value !== 'function')) {
                        return `${field}:${utils_1.escape(value)}`;
                    }
                    if (Array.isArray(value)) {
                        return `${field}: {${recusiveParse('data:', ...value)}}`;
                    }
                    if (typeof value === 'object') {
                        return `${field}: {${recusiveParse('data:', value)}}`;
                    }
                })
                    .filter(v => !!v)
                    .join(',');
            }).map(val => `{${val}}`).join(',');
            if (isArray) {
                innards = `[${innards}]`;
            }
            return `${prefix}${innards}`;
        };
        if (!this._object || !this._object.length) {
            throw new Error('Insert query must have at least one insert object');
        }
        return `${this.alias}:${this.table}(${recusiveParse('objects:', ...this._object)}){affected_rows,returning{${select}}}`;
    }
    apply(...objects) {
        this._object = objects;
        return this;
    }
    join(table, model) {
        return this._join(table, query_1.Query, model);
    }
    aggregate(table, model) {
        return this._join(table, aggregate_1.Aggregate, model);
    }
}
exports.Insert = Insert;
utils_1.applyMixins(Insert, select_1.Select);
class TypedInsert extends Insert {
    constructor(table, alias) {
        super(table, alias);
    }
}
exports.TypedInsert = TypedInsert;
