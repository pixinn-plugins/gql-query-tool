import { GQL_Base, GQL_Model, GQL_Types } from '../gql-base-typings';
import { Aggregate } from './aggregate';
import { Query } from './query';
import { MutationBase } from './mutation-base';
import { Select } from '../parts/select';
import { Where, IWhere } from '../parts/where';
export interface Delete<T extends GQL_Model.Model, RemoveKeys extends keyof Delete<T> = never> extends Omit<Select<T>, 'getSelect' | 'getParsedSelect' | '_join'>, Omit<Where<T>, 'getWhere' | 'getParsedWhere'> {
    select(...params: Parameters<Select<T>['select']>): RemoveQuery<T, RemoveKeys, 'select'>;
    where(where: IWhere<GQL_Types.normalize<T>>): RemoveQuery<T, RemoveKeys, 'where'>;
    join<TableKey extends JoinTables<T>>(table: TableKey | [TableKey, string], model: (m: T[TableKey] extends Array<infer R> ? R extends GQL_Base.ViewOrModel ? Query<R> : never : Omit<Query<T[TableKey], 'limit' | 'offset' | 'where' | 'order'>, 'limit' | 'offset' | 'where' | 'order'>) => void): RemoveQuery<T, RemoveKeys, never>;
    aggregate<TableKey extends AggregateTables<T>, Aggregation extends T[TableKey]>(table: TableKey | [TableKey, string], model: (m: Aggregate<Aggregation extends GQL_Model.Aggregate<infer R> ? R : never, Aggregation>) => void): RemoveQuery<T, RemoveKeys, never>;
}
export declare class Delete<T extends GQL_Model.Model, RemoveKeys extends keyof Delete<T>> extends MutationBase {
    constructor(table: string, alias?: string);
    _parse(): string;
    apply(where?: IWhere<GQL_Types.normalize<T>>): Pick<Delete<T>, Exclude<keyof MutationBase, 'apply'>>;
}
declare type RemoveQuery<T extends GQL_Model.Model, RemoveKeys extends keyof Delete<T>, RemoveKey extends keyof Delete<T>> = Omit<Delete<T, RemoveKeys | RemoveKey>, RemoveKeys | RemoveKey>;
declare type JoinTables<T extends object> = {
    [K in keyof T]: T[K] extends Array<infer R> ? R extends GQL_Base.ViewOrModel ? K : never : T[K] extends GQL_Base.ViewOrModel ? K : never;
}[keyof T];
declare type AggregateTables<T extends object> = {
    [K in keyof T]: T[K] extends GQL_Model.Aggregate<any> ? K : never;
}[keyof T];
declare type Model = GQL_Types.PickValues<{
    [K in keyof GQL_ISchemas]: GQL_ISchemas[K] extends GQL_Model.Delete<infer R> ? R : never;
}>;
export declare class TypedDelete<T extends Model, Key extends keyof Model> extends Delete<T[Key]> {
    constructor(table: Key, alias?: string);
}
export {};
