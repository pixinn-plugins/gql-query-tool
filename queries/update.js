"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypedUpdate = exports.Update = void 0;
const utils_1 = require("../utils");
const mutation_base_1 = require("./mutation-base");
const select_1 = require("../parts/select");
const where_1 = require("../parts/where");
const query_1 = require("./query");
const aggregate_1 = require("./aggregate");
class Update extends mutation_base_1.MutationBase {
    constructor(table, alias) {
        super(table, alias);
        this.join = this.join.bind(this);
        this.aggregate = this.aggregate.bind(this);
        this.set = this.set.bind(this);
        this.increment = this.increment.bind(this);
    }
    _parse() {
        const where = this.getParsedWhere;
        const select = this.getParsedSelect;
        if (!where) {
            throw new Error('Delete mutation must have where clause.');
        }
        if (!this._inc && !this._set) {
            throw new Error('Must supply set or/and increment values.');
        }
        let parsedInc = '';
        if (this._inc) {
            const keys = Object.keys(this._inc);
            if (keys && keys.length) {
                parsedInc = `_inc:{${keys.map(key => {
                    const value = this._inc[key];
                    if (!value) {
                        throw new Error(`Increment value for ${key} field must be set`);
                    }
                    if (typeof value !== 'number' || value !== Math.trunc(value)) {
                        throw new Error(`Increment value for ${key} must be integer`);
                    }
                    return `${key}:${utils_1.escape(value)}`;
                }).join(',')}}`;
            }
        }
        let parsedSet = '';
        if (this._set) {
            const keys = Object.keys(this._set);
            if (keys && keys.length) {
                parsedSet = `_set:{${keys.map(key => {
                    const value = this._set[key];
                    if (!value) {
                        throw new Error(`Set value for ${key} field must be set`);
                    }
                    return `${key}:${utils_1.escape(value)}`;
                }).join(',')}}`;
            }
        }
        return `${this.alias}:${this.table}(${where} ${parsedInc} ${parsedSet}){affected_rows,returning{${select}}}`;
    }
    set(fields) {
        this._set = fields;
        return this;
    }
    increment(fields) {
        this._inc = fields;
        return this;
    }
    apply(where) {
        if (where) {
            this.where(where);
        }
        return this;
    }
    join(table, model) {
        return this._join(table, query_1.Query, model);
    }
    aggregate(table, model) {
        return this._join(table, aggregate_1.Aggregate, model);
    }
}
exports.Update = Update;
utils_1.applyMixins(Update, select_1.Select, where_1.Where);
class TypedUpdate extends Update {
    constructor(table, alias) {
        super(table, alias);
    }
}
exports.TypedUpdate = TypedUpdate;
