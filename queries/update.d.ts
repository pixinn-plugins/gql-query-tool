import { GQL_Model, GQL_Base, GQL_Types } from '../gql-base-typings';
import { MutationBase } from './mutation-base';
import { Select } from '../parts/select';
import { Where, IWhere } from '../parts/where';
import { Query } from './query';
import { Aggregate } from './aggregate';
export interface Update<T extends GQL_Model.Model, RemoveKeys extends keyof Update<T> = never> extends Omit<Select<T>, 'getSelect' | 'getParsedSelect' | '_join'>, Omit<Where<T>, 'getWhere' | 'getParsedWhere'> {
    select(...params: Parameters<Select<T>['select']>): RemoveQuery<T, RemoveKeys, 'select'>;
    where(where: IWhere<GQL_Types.normalize<T>>): RemoveQuery<T, RemoveKeys, 'where'>;
    join<TableKey extends JoinTables<T>>(table: TableKey | [TableKey, string], model: (m: T[TableKey] extends Array<infer R> ? R extends GQL_Base.ViewOrModel ? Query<R> : never : Omit<Query<T[TableKey], 'limit' | 'offset' | 'where' | 'order'>, 'limit' | 'offset' | 'where' | 'order'>) => void): RemoveQuery<T, RemoveKeys, never>;
    aggregate<TableKey extends AggregateTables<T>, Aggregation extends T[TableKey]>(table: TableKey | [TableKey, string], model: (m: Aggregate<Aggregation extends GQL_Model.Aggregate<infer R> ? R : never, Aggregation>) => void): RemoveQuery<T, RemoveKeys, never>;
}
declare type RemoveQuery<T extends GQL_Model.Model, RemoveKeys extends keyof Update<T>, RemoveKey extends keyof Update<T>> = Omit<Update<T, RemoveKeys | RemoveKey>, RemoveKeys | RemoveKey>;
declare type JoinTables<T extends object> = {
    [K in keyof T]: T[K] extends Array<infer R> ? R extends GQL_Base.ViewOrModel ? K : never : T[K] extends GQL_Base.ViewOrModel ? K : never;
}[keyof T];
declare type AggregateTables<T extends object> = {
    [K in keyof T]: T[K] extends GQL_Model.Aggregate<any> ? K : never;
}[keyof T];
declare type IncrementType<T extends object> = Partial<GQL_Types.normalize<GQL_Types.PickValues<GQL_Types.RemoveTypename<Required<{
    [K in keyof T]: NonNullable<T[K]> extends GQL_Types.integer ? T[K] : never;
}>>>>>;
declare type SetType<T extends object> = Partial<GQL_Types.normalize<GQL_Types.RemoveTypename<GQL_Types.SimpleFields<T>>>>;
export declare class Update<T extends GQL_Model.Model, RemoveKeys extends keyof Update<T>> extends MutationBase {
    private _inc?;
    private _set?;
    constructor(table: string, alias?: string);
    _parse(): string;
    set(fields: SetType<T>): RemoveQuery<T, RemoveKeys, 'set'>;
    increment(fields: IncrementType<T>): RemoveQuery<T, RemoveKeys, 'increment'>;
    apply(where?: IWhere<GQL_Types.normalize<T>>): Pick<Update<T, RemoveKeys>, keyof GQL_Base.Parseable>;
}
declare type Model = GQL_Types.PickValues<{
    [K in keyof GQL_ISchemas]: GQL_ISchemas[K] extends GQL_Model.Update<infer R> ? R : never;
}>;
export declare class TypedUpdate<T extends Model, Key extends keyof Model> extends Update<T[Key]> {
    constructor(table: Key, alias?: string);
}
export {};
