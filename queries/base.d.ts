import { GQL_Base } from '../gql-base-typings';
export declare abstract class QueryBase implements GQL_Base.Parseable {
    protected table: string;
    protected alias: string;
    constructor(table: string, alias?: string);
    abstract _parse(): string;
}
