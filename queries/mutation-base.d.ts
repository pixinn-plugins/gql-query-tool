import { QueryBase } from './base';
export declare abstract class MutationBase extends QueryBase {
    constructor(table: string, alias?: string);
    abstract apply(...args: any[]): void;
}
