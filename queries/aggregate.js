"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypedAggregate = exports.Aggregate = void 0;
const utils_1 = require("../utils");
const limit_1 = require("../parts/limit");
const offset_1 = require("../parts/offset");
const order_1 = require("../parts/order");
const where_1 = require("../parts/where");
const select_1 = require("../parts/select");
const base_1 = require("./base");
class Aggregate extends base_1.QueryBase {
    count(alias) {
        this.select(['count', alias || 'count']);
        return this;
    }
    min(field, alias) {
        this.select([`min{${alias || field}:${field}}`, 'min']);
        return this;
    }
    max(field, alias) {
        this.select([`max{${alias || field}:${field}}`, 'max']);
        return this;
    }
    avg(field, alias) {
        this.select([`avg{${alias || field}:${field}}`, 'avg']);
        return this;
    }
    sum(field, alias) {
        this.select([`sum{${alias || field}:${field}}`, 'sum']);
        return this;
    }
    _parse() {
        let strBuild = `${this.alias}:${this.table}`;
        const limit = this.getParsedLimit;
        const offset = this.getParsedOffset;
        const where = this.getParsedWhere;
        const order = this.getParsedOrder;
        if (!!limit || !!offset || !!where || !!order) {
            let strParams = '';
            if (limit)
                strParams += limit + ',';
            if (offset)
                strParams += offset + ',';
            if (where)
                strParams += where + ',';
            if (order)
                strParams += order;
            strBuild += `(${strParams})`;
        }
        const select = this.getParsedSelect;
        strBuild += `{aggregate{${select}}}`;
        return strBuild;
    }
}
exports.Aggregate = Aggregate;
utils_1.applyMixins(Aggregate, select_1.Select, limit_1.Limit, offset_1.Offset, order_1.Order, where_1.Where);
class TypedAggregate extends Aggregate {
    constructor(table, alias) {
        super(table, alias);
    }
}
exports.TypedAggregate = TypedAggregate;
