import { GQL_Base, GQL_Model, GQL_Types } from '../gql-base-typings';
import { Select } from '../parts/select';
import { QueryBase } from './base';
import { Limit } from '../parts/limit';
import { Offset } from '../parts/offset';
import { Where, IWhere } from '../parts/where';
import { Order } from '../parts/order';
import { Aggregate } from './aggregate';
export interface Query<T extends GQL_Base.ViewOrModel, RemoveKeys extends keyof Query<T> = never> extends Omit<Select<T>, 'getSelect' | 'getParsedSelect' | '_join'>, Omit<Limit, 'getLimit' | 'getParsedLimit'>, Omit<Offset, 'getOffset' | 'getParsedOffset'>, Omit<Where<T>, 'getWhere' | 'getParsedWhere'>, Omit<Order<T>, 'getOrder' | 'getParsedOrder'> {
    limit(...params: Parameters<Limit['limit']>): RemoveQuery<T, RemoveKeys, 'limit'>;
    offset(...params: Parameters<Offset['offset']>): RemoveQuery<T, RemoveKeys, 'offset'>;
    where(where: IWhere<GQL_Types.normalize<T>>): RemoveQuery<T, RemoveKeys, 'where'>;
    order(...params: Parameters<Order<T>['order']>): RemoveQuery<T, RemoveKeys, 'order'>;
    select(...params: Parameters<Select<T>['select']>): RemoveQuery<T, RemoveKeys, 'select'>;
    join<TableKey extends JoinTables<T>>(table: TableKey | [TableKey, string], model: (m: T[TableKey] extends Array<infer R> ? R extends GQL_Base.ViewOrModel ? Query<R> : never : RemoveQuery<T[TableKey], never, 'limit' | 'offset' | 'where' | 'order'>) => void): RemoveQuery<T, RemoveKeys, never>;
    aggregate<TableKey extends AggregateTables<T>, Aggregation extends T[TableKey]>(table: TableKey | [TableKey, string], model: (m: Aggregate<Aggregation extends GQL_Model.Aggregate<infer R> ? R : never, Aggregation>) => void): RemoveQuery<T, RemoveKeys, never>;
}
declare type RemoveQuery<T extends GQL_Base.ViewOrModel, RemoveKeys extends keyof Query<T>, RemoveKey extends keyof Query<T>> = Omit<Query<T, RemoveKeys | RemoveKey>, RemoveKeys | RemoveKey>;
declare type JoinTables<T extends object> = {
    [K in keyof T]: T[K] extends Array<infer R> ? R extends GQL_Base.ViewOrModel ? K : never : T[K] extends GQL_Base.ViewOrModel ? K : never;
}[keyof T];
declare type AggregateTables<T extends object> = {
    [K in keyof T]: T[K] extends GQL_Model.Aggregate<any> ? K : never;
}[keyof T];
export declare class Query<T extends GQL_Base.ViewOrModel, RemoveKeys extends keyof Query<T>> extends QueryBase {
    constructor(table: string, alias?: string);
    _parse(): string;
}
declare type ViewOrModel = GQL_Types.PickValues<{
    [K in keyof GQL_ISchemas]: GQL_ISchemas[K] extends GQL_Base.ViewOrModel ? GQL_ISchemas[K] : never;
}>;
export declare class TypedQuery<T extends ViewOrModel, Key extends keyof ViewOrModel> extends Query<T[Key]> {
    constructor(table: Key, alias?: string);
}
export {};
