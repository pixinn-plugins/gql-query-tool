"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryBase = void 0;
const utils_1 = require("../utils");
class QueryBase {
    constructor(table, alias) {
        this.table = table;
        this.alias = alias || table;
        if (!utils_1.aliasValidityRegex.test(this.alias)) {
            throw new Error('Transaction name format is invalid.');
        }
        this._parse = this._parse.bind(this);
    }
}
exports.QueryBase = QueryBase;
