"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypedQuery = exports.Query = void 0;
const utils_1 = require("../utils");
const select_1 = require("../parts/select");
const base_1 = require("./base");
const limit_1 = require("../parts/limit");
const offset_1 = require("../parts/offset");
const where_1 = require("../parts/where");
const order_1 = require("../parts/order");
const aggregate_1 = require("./aggregate");
class Query extends base_1.QueryBase {
    constructor(table, alias) {
        super(table, alias);
        this.join = this.join.bind(this);
        this.aggregate = this.aggregate.bind(this);
    }
    _parse() {
        let strBuild = `${this.alias}:${this.table}`;
        const limit = this.getParsedLimit;
        const offset = this.getParsedOffset;
        const where = this.getParsedWhere;
        const order = this.getParsedOrder;
        if (!!limit || !!offset || !!where || !!order) {
            let strParams = '';
            if (limit)
                strParams += limit + ',';
            if (offset)
                strParams += offset + ',';
            if (where)
                strParams += where + ',';
            if (order)
                strParams += order;
            strBuild += `(${strParams})`;
        }
        const select = this.getParsedSelect;
        strBuild += `{${select}}`;
        return strBuild;
    }
    join(table, model) {
        return this._join(table, Query, model);
    }
    aggregate(table, model) {
        return this._join(table, aggregate_1.Aggregate, model);
    }
}
exports.Query = Query;
class TypedQuery extends Query {
    constructor(table, alias) {
        super(table, alias);
    }
}
exports.TypedQuery = TypedQuery;
utils_1.applyMixins(Query, select_1.Select, limit_1.Limit, offset_1.Offset, where_1.Where, order_1.Order);
