import { Limit } from '../parts/limit';
import { Offset } from '../parts/offset';
import { Order } from '../parts/order';
import { Where, IWhere } from '../parts/where';
import { Select } from '../parts/select';
import { QueryBase } from './base';
import { GQL_Model, GQL_Types } from '../gql-base-typings';
export interface Aggregate<Schema extends GQL_Model.Model, T extends GQL_Model.Aggregate<Schema>, RemoveKeys extends keyof Aggregate<Schema, T> = never> extends Omit<Limit, 'getLimit' | 'getParsedLimit'>, Omit<Offset, 'getOffset' | 'getParsedOffset'>, Omit<Order<Schema>, 'getOrder' | 'getParsedOrder'>, Omit<Where<Schema>, 'getWhere' | 'getParsedWhere'>, Omit<Select<any>, keyof Select<any>> {
    limit(...params: Parameters<Limit['limit']>): RemoveAgg<Schema, T, RemoveKeys, 'limit'>;
    offset(...params: Parameters<Offset['offset']>): RemoveAgg<Schema, T, RemoveKeys, 'offset'>;
    where(where: IWhere<GQL_Types.normalize<T>>): RemoveAgg<Schema, T, RemoveKeys, 'where'>;
    order(...params: Parameters<Order<Schema>['order']>): RemoveAgg<Schema, T, RemoveKeys, 'order'>;
}
declare type RemoveAgg<Schema extends GQL_Model.Model, T extends GQL_Model.Aggregate<Schema>, RemoveKeys extends keyof Aggregate<Schema, T>, RemoveKey extends keyof Aggregate<Schema, T>> = Omit<Aggregate<Schema, T, RemoveKeys | RemoveKey>, RemoveKeys | RemoveKey>;
export declare class Aggregate<Schema extends GQL_Model.Model, T extends GQL_Model.Aggregate<Schema>, RemoveKeys extends keyof Aggregate<Schema, T> = never> extends QueryBase {
    count(alias?: string): RemoveAgg<Schema, T, RemoveKeys, 'count'>;
    min(field: GQL_Types.Keys<Schema>, alias?: string): RemoveAgg<Schema, T, RemoveKeys, never>;
    max(field: GQL_Types.Keys<Schema>, alias?: string): RemoveAgg<Schema, T, RemoveKeys, never>;
    avg(field: NumericFields<Schema>, alias?: string): RemoveAgg<Schema, T, RemoveKeys, never>;
    sum(field: NumericFields<Schema>, alias?: string): RemoveAgg<Schema, T, RemoveKeys, never>;
    _parse(): string;
}
declare type NumericFields<T extends object> = GQL_Types.RemoveTypename<{
    [K in keyof T]: NonNullable<T[K]> extends number ? K : never;
}[keyof T]>;
declare type Aggregations = GQL_Types.PickValues<{
    [K in keyof GQL_ISchemas]: GQL_ISchemas[K] extends GQL_Model.Aggregate<infer R> ? R : never;
}>;
export declare class TypedAggregate<T extends Aggregations, Key extends keyof Aggregations> extends Aggregate<T[Key], GQL_ISchemas[Key]> {
    constructor(table: Key, alias?: string);
}
export {};
