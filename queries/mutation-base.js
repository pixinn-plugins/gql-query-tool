"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MutationBase = void 0;
const base_1 = require("./base");
class MutationBase extends base_1.QueryBase {
    constructor(table, alias) {
        super(table, alias);
    }
}
exports.MutationBase = MutationBase;
