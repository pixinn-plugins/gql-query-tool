import { GQL_Model, GQL_Base, GQL_Types } from '../gql-base-typings';
import { MutationBase } from './mutation-base';
import { Query } from './query';
import { Aggregate } from './aggregate';
import { Select } from '../parts/select';
export interface Insert<T extends GQL_Model.Model, RemoveKeys extends keyof Insert<T> = never> extends Omit<Select<T>, 'getSelect' | 'getParsedSelect' | '_join'> {
    select(...params: Parameters<Select<T>['select']>): RemoveQuery<T, RemoveKeys, 'select'>;
    join<TableKey extends JoinTables<T>>(table: TableKey | [TableKey, string], model: (m: T[TableKey] extends Array<infer R> ? R extends GQL_Base.ViewOrModel ? Query<R> : never : Omit<Query<T[TableKey], 'limit' | 'offset' | 'where' | 'order'>, 'limit' | 'offset' | 'where' | 'order'>) => void): RemoveQuery<T, RemoveKeys, never>;
    aggregate<TableKey extends AggregateTables<T>, Aggregation extends T[TableKey]>(table: TableKey | [TableKey, string], model: (m: Aggregate<Aggregation extends GQL_Model.Aggregate<infer R> ? R : never, Aggregation>) => void): RemoveQuery<T, RemoveKeys, never>;
}
export declare class Insert<T extends GQL_Model.Model, RemoveKeys extends keyof Insert<T>> extends MutationBase {
    private _object?;
    constructor(table: string, alias?: string);
    _parse(): string;
    apply(...objects: Array<InsertType<T>>): Pick<Insert<T, RemoveKeys>, keyof GQL_Base.Parseable>;
}
declare type RemoveQuery<T extends GQL_Model.Model, RemoveKeys extends keyof Insert<T>, RemoveKey extends keyof Insert<T>> = Omit<Insert<T, RemoveKeys | RemoveKey>, RemoveKeys | RemoveKey>;
declare type JoinTables<T extends object> = {
    [K in keyof T]: T[K] extends Array<infer R> ? R extends GQL_Base.ViewOrModel ? K : never : T[K] extends GQL_Base.ViewOrModel ? K : never;
}[keyof T];
declare type AggregateTables<T extends object> = {
    [K in keyof T]: T[K] extends GQL_Model.Aggregate<any> ? K : never;
}[keyof T];
declare type InsertManipulationChain<T extends object> = GQL_Types.RemoveTypename<GQL_Types.normalize<OptionalNesting<RemoveInsertAggregates<T>>>>;
declare type InsertTypeObject<T extends object> = {
    [K in keyof T]: NonNullable<T[K]> extends Array<infer R> ? R extends object ? Array<InsertTypeObject<InsertManipulationChain<R>>> : R[] : NonNullable<T[K]> extends object ? InsertTypeObject<InsertManipulationChain<NonNullable<T[K]>>> : T[K];
};
declare type RemoveInsertAggregates<T extends object> = Omit<T, Exclude<{
    [K in keyof T]: NonNullable<T[K]> extends GQL_Model.Aggregate<any> ? K : never;
}[keyof T], undefined>>;
declare type OptionalNesting<T extends object> = Partial<Pick<T, {
    [K in keyof T]: T[K] extends Array<infer R> ? R extends GQL_Model.Model ? K : never : T[K] extends GQL_Model.Model ? K : never;
}[keyof T]>> & Pick<T, {
    [K in keyof T]: T[K] extends Array<infer R> ? R extends GQL_Model.Model ? never : K : T[K] extends GQL_Model.Model ? never : K;
}[keyof T]>;
declare type InsertType<T extends object> = InsertTypeObject<InsertManipulationChain<T>>;
declare type Model = GQL_Types.PickValues<{
    [K in keyof GQL_ISchemas]: GQL_ISchemas[K] extends GQL_Model.Insert<infer R> ? R : never;
}>;
export declare class TypedInsert<T extends Model, Key extends keyof Model> extends Insert<T[Key]> {
    constructor(table: Key, alias?: string);
}
export {};
