"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypedDelete = exports.Delete = void 0;
const utils_1 = require("../utils");
const aggregate_1 = require("./aggregate");
const query_1 = require("./query");
const mutation_base_1 = require("./mutation-base");
const select_1 = require("../parts/select");
const where_1 = require("../parts/where");
class Delete extends mutation_base_1.MutationBase {
    constructor(table, alias) {
        super(table, alias);
        this.join = this.join.bind(this);
        this.aggregate = this.aggregate.bind(this);
    }
    _parse() {
        const where = this.getParsedWhere;
        const select = this.getParsedSelect;
        if (!where) {
            throw new Error('Delete mutation must have where clause.');
        }
        return `${this.alias}:${this.table}(${where}){affected_rows,returning{${select}}}`;
    }
    apply(where) {
        if (where) {
            this.where(where);
        }
        return this;
    }
    join(table, model) {
        return this._join(table, query_1.Query, model);
    }
    aggregate(table, model) {
        return this._join(table, aggregate_1.Aggregate, model);
    }
}
exports.Delete = Delete;
utils_1.applyMixins(Delete, select_1.Select, where_1.Where);
class TypedDelete extends Delete {
    constructor(table, alias) {
        super(table, alias);
    }
}
exports.TypedDelete = TypedDelete;
