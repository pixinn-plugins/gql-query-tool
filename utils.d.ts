export declare function escape(value: any): any;
export declare function applyMixins(derivedCtor: any, ...constructors: any[]): void;
export declare const aliasValidityRegex: RegExp;
