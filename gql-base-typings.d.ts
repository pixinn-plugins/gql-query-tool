export declare namespace GQL_Base {
    interface ViewOrModel {
        __typename: 'Model' | 'View';
    }
    interface ViewOrModelOrAggregate {
        __typename: 'Model' | 'View' | 'Aggregate';
    }
    interface Parseable<T = string> {
        _parse(): T;
    }
}
export declare namespace GQL_Types {
    /** Set of simple JavaScript types. */
    type SimpleTypes = string | number | boolean | Date | null | undefined;
    /** Removes never type keys from object */
    type PickValues<T extends object> = Pick<T, {
        [K in keyof T]: T[K] extends never ? never : K;
    }[keyof T]>;
    /** Removes __typename fields from GQL Models or Views */
    type RemoveTypename<T> = T extends GQL_Base.ViewOrModel ? Omit<T, keyof GQL_Base.ViewOrModel> : T;
    /** Returns simple type field keys from object  */
    type Keys<T extends object> = {
        [K in keyof T]: T[K] extends never ? never : T[K] extends SimpleTypes ? K : never;
    }[Exclude<keyof T, keyof GQL_Base.ViewOrModel>];
    /** Return object and array field keys from object */
    type Objects<T extends object> = {
        [K in keyof T]: T[K] extends SimpleTypes ? never : K;
    }[Exclude<keyof T, keyof GQL_Base.ViewOrModel>];
    /** Return object with non object or array fields */
    type SimpleFields<T extends object> = PickValues<{
        [K in keyof T]: T[K] extends SimpleTypes ? T[K] : never;
    }>;
    type normalize<T extends object> = {
        [K in keyof T]: NonNullable<T[K]> extends GQL_Types.integer ? number : T[K];
    };
    type integer = number & {
        __typename: 'integer';
    };
}
export declare namespace GQL_Model {
    interface Model {
        __typename: 'Model';
    }
    interface View {
        __typename: 'View';
    }
    interface Aggregate<T extends Model> {
        __typename: 'Aggregate';
    }
    interface Delete<T extends Model> {
        __typename: 'Delete';
    }
    interface Update<T extends Model> {
        __typename: 'Update';
    }
    interface Insert<T extends Model> {
        __typename: 'Insert';
    }
    interface Timestamps {
        created_at: Date;
        updated_at: Date;
    }
    interface SoftDelete {
        deleted_at?: Date;
    }
}
