export declare namespace Model {
    interface Model {
        __typename: 'Model';
    }
    interface View {
        __typename: 'View';
    }
    interface Aggregate<T extends Model> {
        __typename: 'Aggregate';
    }
    interface Delete<T extends Model> {
        __typename: 'Delete';
    }
    interface Update<T extends Model> {
        __typename: 'Update';
    }
    interface Insert<T extends Model> {
        __typename: 'Insert';
    }

    interface Timestamps {
        created_at: Date;
        updated_at: Date;
    }
    interface SoftDelete {
        deleted_at?: Date;
    }
}
export declare namespace Types {
    type RemoveTypename<T> = T extends { __typename: any } ? Omit<T, '__typename'> : T;
    type normalize<T extends object> = { [K in keyof T]: NonNullable<T[K]> extends integer ? number : T[K] };
    type integer = number & { __typename: 'integer' };
}

export as namespace GQL;
